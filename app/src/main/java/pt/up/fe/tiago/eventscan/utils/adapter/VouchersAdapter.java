package pt.up.fe.tiago.eventscan.utils.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Voucher;

public class VouchersAdapter extends RecyclerView.Adapter<VouchersAdapter.VoucherHolder> {
    private List<Voucher> vouchers;

    static class VoucherHolder extends RecyclerView.ViewHolder {
        ImageView voucherImage;
        TextView voucherTitle;

        VoucherHolder(View v) {
            super(v);
            this.voucherImage = v.findViewById(R.id.voucher_image);
            this.voucherTitle = v.findViewById(R.id.voucher_title);
        }
    }

    public VouchersAdapter(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    @NonNull
    @Override
    public VouchersAdapter.VoucherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.voucher_card_item, parent, false);
        return new VoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VoucherHolder holder, int position) {

        holder.voucherImage.setImageResource(getImageResourceBasedOnProductCode(vouchers.get(position).getProductCode()));
        if(vouchers.get(position).getProductCode().toLowerCase().contains("fivepercent"))
            holder.voucherTitle.setText(String.valueOf("Discount"));
        else
            holder.voucherTitle.setText(String.valueOf("Free"));
    }

    private int getImageResourceBasedOnProductCode(String productCode) {
        if(productCode.toLowerCase().contains("popcorn"))
            return R.drawable.ic_popcorn;
        else if(productCode.toLowerCase().contains("coffee"))
            return R.drawable.ic_coffee_for_voucher;
        else
            return R.drawable.ic_five_percent;
    }

    public void addVouchers(List<Voucher> vouchers) {
        for(int i = 0; i < vouchers.size(); i++) {
            this.vouchers.add(vouchers.get(i));
            notifyItemInserted(this.vouchers.indexOf(vouchers.get(i)));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return vouchers.size();
    }
}


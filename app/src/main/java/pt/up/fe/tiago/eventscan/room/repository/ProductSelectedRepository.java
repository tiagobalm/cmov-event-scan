package pt.up.fe.tiago.eventscan.room.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.EventScanDatabase;
import pt.up.fe.tiago.eventscan.room.dao.ProductSelectedDao;
import pt.up.fe.tiago.eventscan.room.entity.ProductSelected;

public class ProductSelectedRepository {
    private ProductSelectedDao productSelectedDao;
    private LiveData<List<ProductSelected>> allProducts;

    public ProductSelectedRepository(Application application) {
        EventScanDatabase eventScanDatabase = EventScanDatabase.getInstance(application);
        productSelectedDao = eventScanDatabase.productSelectedDao();
        allProducts = productSelectedDao.getAllProducts();
    }

    public void insert(ProductSelected productSelected) {
        new InsertProduct(productSelectedDao).execute(productSelected);
    }

    public void update(ProductSelected productSelected) {
        new UpdateProduct(productSelectedDao).execute(productSelected);
    }

    public void delete(ProductSelected productSelected) {
        new DeleteProduct(productSelectedDao).execute(productSelected);
    }

    public LiveData<List<ProductSelected>> getAllProducts() {
        return allProducts;
    }

    public LiveData<ProductSelected> getProductByUUID(Integer uuid) { return productSelectedDao.getProductByUUID(uuid); }

    public void deleteAllProducts() {
        new DeleteAllProducts(productSelectedDao).execute();
    }

    public void deleteByUUID(Integer uuid) {
        new DeleteByUUID(productSelectedDao).execute(uuid);
    }

    private static class InsertProduct extends AsyncTask<ProductSelected, Void, Void> {
        private ProductSelectedDao productSelectedDao;

        private InsertProduct(ProductSelectedDao productSelectedDao) {
            this.productSelectedDao = productSelectedDao;
        }

        @Override
        protected Void doInBackground(ProductSelected... productSelecteds) {
            productSelectedDao.insert(productSelecteds[0]);
            return null;
        }
    }

    private static class UpdateProduct extends AsyncTask<ProductSelected, Void, Void> {
        private ProductSelectedDao productSelectedDao;

        private UpdateProduct(ProductSelectedDao productSelectedDao) {
            this.productSelectedDao = productSelectedDao;
        }

        @Override
        protected Void doInBackground(ProductSelected... productSelecteds) {
            productSelectedDao.update(productSelecteds[0]);
            return null;
        }
    }

    private static class DeleteProduct extends AsyncTask<ProductSelected, Void, Void> {
        private ProductSelectedDao productSelectedDao;

        private DeleteProduct(ProductSelectedDao productSelectedDao) {
            this.productSelectedDao = productSelectedDao;
        }

        @Override
        protected Void doInBackground(ProductSelected... productSelecteds) {
            productSelectedDao.delete(productSelecteds[0]);
            return null;
        }
    }

    private static class DeleteAllProducts extends AsyncTask<Void, Void, Void> {
        private ProductSelectedDao productSelectedDao;

        private DeleteAllProducts(ProductSelectedDao productSelectedDao) {
            this.productSelectedDao = productSelectedDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productSelectedDao.deleteAllProducts();
            return null;
        }
    }

    private static class DeleteByUUID extends AsyncTask<Integer, Void, Void> {
        private ProductSelectedDao productSelectedDao;

        private DeleteByUUID(ProductSelectedDao productSelectedDao) {
            this.productSelectedDao = productSelectedDao;
        }

        @Override
        protected Void doInBackground(Integer... uuid) {
            productSelectedDao.deleteByUUID(uuid[0]);
            return null;
        }
    }
}

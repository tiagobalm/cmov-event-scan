package pt.up.fe.tiago.eventscan.webservice.dto;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class CafeteriaOrderDTO implements Serializable {
    private UUID customerUUID;
    private List<Integer> productsIDs;
    private List<Integer> numberProducts;
    private List<UUID> vouchers;

    public CafeteriaOrderDTO() {}

    public CafeteriaOrderDTO(UUID customerUUID, List<Integer> productsIDs, List<Integer> numberProducts, List<UUID> vouchers) {
        this.customerUUID = customerUUID;
        this.productsIDs = productsIDs;
        this.numberProducts = numberProducts;
        this.vouchers = vouchers;
    }

    public UUID getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(UUID customerUUID) {
        this.customerUUID = customerUUID;
    }

    public List<Integer> getProducts() {
        return productsIDs;
    }

    public void setProducts(List<Integer> products) {
        this.productsIDs = products;
    }

    public List<Integer> getNumberProducts() {
        return numberProducts;
    }

    public void setNumberProducts(List<Integer> numberProducts) {
        this.numberProducts = numberProducts;
    }

    public List<UUID> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<UUID> vouchers) {
        this.vouchers = vouchers;
    }
}

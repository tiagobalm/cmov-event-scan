package pt.up.fe.tiago.eventscan.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ProductSelected")
public class ProductSelected {

    @PrimaryKey
    private Integer uuid;
    private String name;
    private Double price;
    private Integer numberOfProducts;

    public ProductSelected(Integer uuid, String name, Double price, Integer numberOfProducts) {
        this.uuid = uuid;
        this.name = name;
        this.price = price;
        this.numberOfProducts = numberOfProducts;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(Integer numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }
}

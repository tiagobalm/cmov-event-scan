package pt.up.fe.tiago.eventscan.room.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.EventScanDatabase;
import pt.up.fe.tiago.eventscan.room.dao.EventDao;
import pt.up.fe.tiago.eventscan.room.dao.ProductDao;
import pt.up.fe.tiago.eventscan.room.entity.Event;
import pt.up.fe.tiago.eventscan.room.entity.Product;

public class ProductRepository {
    private ProductDao productDao;
    private LiveData<List<Product>> allEvents;

    public ProductRepository(Application application) {
        EventScanDatabase eventScanDatabase = EventScanDatabase.getInstance(application);
        productDao = eventScanDatabase.productDao();
        allEvents = productDao.getAllProducts();
    }

    public void insert(Product product) {
        new InsertProduct(productDao).execute(product);
    }

    public void update(Product product) {
        new UpdateProduct(productDao).execute(product);
    }

    public void delete(Product product) {
        new DeleteProduct(productDao).execute(product);
    }

    public LiveData<List<Product>> getAllProducts() {
        return allEvents;
    }

    public LiveData<Product> getProductByUUID(Integer uuid) { return productDao.getProductByUUID(uuid); }

    public void deleteAllProducts() {
        new DeleteAllProducts(productDao).execute();
    }

    public void deleteByUUID(Integer uuid) {
        new DeleteByUUID(productDao).execute(uuid);
    }

    private static class InsertProduct extends AsyncTask<Product, Void, Void> {
        private ProductDao productDao;

        private InsertProduct(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Product... products) {
            productDao.insert(products[0]);
            return null;
        }
    }

    private static class UpdateProduct extends AsyncTask<Product, Void, Void> {
        private ProductDao productDao;

        private UpdateProduct(ProductDao eventDao) {
            this.productDao = eventDao;
        }

        @Override
        protected Void doInBackground(Product... products) {
            productDao.update(products[0]);
            return null;
        }
    }

    private static class DeleteProduct extends AsyncTask<Product, Void, Void> {
        private ProductDao productDao;

        private DeleteProduct(ProductDao eventDao) {
            this.productDao = eventDao;
        }

        @Override
        protected Void doInBackground(Product... products) {
            productDao.delete(products[0]);
            return null;
        }
    }

    private static class DeleteAllProducts extends AsyncTask<Void, Void, Void> {
        private ProductDao productDao;

        private DeleteAllProducts(ProductDao productDao) {
            this.productDao = productDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productDao.deleteAllProducts();
            return null;
        }
    }

    private static class DeleteByUUID extends AsyncTask<Integer, Void, Void> {
        private ProductDao productDao;

        private DeleteByUUID(ProductDao eventDao) {
            this.productDao = eventDao;
        }

        @Override
        protected Void doInBackground(Integer... uuid) {
            productDao.deleteByUUID(uuid[0]);
            return null;
        }
    }
}

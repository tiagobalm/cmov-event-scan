package pt.up.fe.tiago.eventscan.room.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import pt.up.fe.tiago.eventscan.room.entity.ProductSelected;

@Dao
public interface ProductSelectedDao {

    @Insert
    void insert(ProductSelected event);

    @Update
    void update(ProductSelected event);

    @Delete
    void delete(ProductSelected event);

    @Query("SELECT * FROM ProductSelected")
    LiveData<List<ProductSelected>> getAllProducts();

    @Query("SELECT * FROM ProductSelected WHERE uuid = :uuid")
    LiveData<ProductSelected> getProductByUUID(Integer uuid);

    @Query("DELETE FROM ProductSelected")
    void deleteAllProducts();

    @Query("DELETE FROM ProductSelected WHERE uuid = :uuid")
    void deleteByUUID(Integer uuid);
}

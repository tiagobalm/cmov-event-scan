package pt.up.fe.tiago.eventscan.utils.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.fragment.profile.PreviousCafeteriaFragment;
import pt.up.fe.tiago.eventscan.utils.UtilsFunctions;
import pt.up.fe.tiago.eventscan.webservice.dto.CafeteriaOrderResultDTO;

public class ProfileCafeteriaAdapter extends RecyclerView.Adapter<ProfileCafeteriaAdapter.ProductHolder> {
    private List<CafeteriaOrderResultDTO> cafeteriaOrderResultDTOS;
    private PreviousCafeteriaFragment previousCafeteriaFragment;

    static class ProductHolder extends RecyclerView.ViewHolder {
        ImageView voucherOneImage, voucherTwoImage;
        TextView voucherOneTitle, voucherTwoTitle, totalPrice, orderNumber;
        RecyclerView productList;

        ProductHolder(View v) {
            super(v);
            this.voucherOneImage = v.findViewById(R.id.voucher_image_one);
            this.voucherTwoImage = v.findViewById(R.id.voucher_image_two);
            this.voucherOneTitle = v.findViewById(R.id.voucher_title_one);
            this.voucherTwoTitle = v.findViewById(R.id.voucher_title_two);
            this.orderNumber = v.findViewById(R.id.order_number);
            this.totalPrice = v.findViewById(R.id.total_price);
            this.productList = v.findViewById(R.id.product_list);
        }
    }

    public ProfileCafeteriaAdapter(List<CafeteriaOrderResultDTO> cafeteriaOrderResultDTOS, PreviousCafeteriaFragment previousCafeteriaFragment) {
        this.cafeteriaOrderResultDTOS = cafeteriaOrderResultDTOS;
        this.previousCafeteriaFragment = previousCafeteriaFragment;
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_orders_card, parent, false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        CafeteriaOrderResultDTO cafeteriaOrderResultDTO = cafeteriaOrderResultDTOS.get(position);

        if(cafeteriaOrderResultDTO.getVouchers().size() >= 1)
            UtilsFunctions.fillVoucher(cafeteriaOrderResultDTO.getVouchers().get(0).getProductCode(), holder.voucherOneImage, holder.voucherOneTitle);
        else {
            holder.voucherOneImage.setImageResource(R.drawable.ic_discount_voucher_100dp);
            holder.voucherOneTitle.setText(String.valueOf("No voucher applied."));
        }

        if(cafeteriaOrderResultDTO.getVouchers().size() >= 2)
            UtilsFunctions.fillVoucher(cafeteriaOrderResultDTO.getVouchers().get(1).getProductCode(), holder.voucherTwoImage, holder.voucherTwoTitle);
        else {
            holder.voucherTwoImage.setImageResource(R.drawable.ic_discount_voucher_100dp);
            holder.voucherTwoTitle.setText(String.valueOf("No voucher applied."));
        }

        ProfileCafeteriaProductAdapter profileCafeteriaProductAdapter = new ProfileCafeteriaProductAdapter(cafeteriaOrderResultDTO.getProducts(), cafeteriaOrderResultDTO.getNumberOfProducts());
        holder.productList.setLayoutManager(new LinearLayoutManager(previousCafeteriaFragment.getContext()));
        holder.productList.setAdapter(profileCafeteriaProductAdapter);
        ViewCompat.setNestedScrollingEnabled(holder.productList, false);

        holder.totalPrice.setText(String.valueOf(cafeteriaOrderResultDTO.getTotalPrice() + "$"));
        holder.orderNumber.setText(String.valueOf("Order: #" + cafeteriaOrderResultDTO.getInvoiceID()));
    }

    public void addOrderInformation(List<CafeteriaOrderResultDTO> cafeteriaOrderResultDTOS) {
        for(CafeteriaOrderResultDTO cafeteriaOrderResultDTO : cafeteriaOrderResultDTOS) {
            System.out.println(cafeteriaOrderResultDTO);
            this.cafeteriaOrderResultDTOS.add(cafeteriaOrderResultDTO);
            notifyItemInserted(this.cafeteriaOrderResultDTOS.indexOf(cafeteriaOrderResultDTO));
        }
    }

    @Override
    public int getItemCount() {
        return cafeteriaOrderResultDTOS.size();
    }
}


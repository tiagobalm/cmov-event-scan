package pt.up.fe.tiago.eventscan.fragment.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import pt.up.fe.tiago.eventscan.R;

public class ProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BottomNavigationView bottomNavigationView = view.findViewById(R.id.profile_navigation);
        NavigationUI.setupWithNavController(bottomNavigationView, Navigation.findNavController(view.findViewById(R.id.profile_host)));

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
        ((TextView)view.findViewById(R.id.profile_title)).setText(sharedPreferences.getString(getResources().getString(R.string.NAME_TAG), ""));
    }
}

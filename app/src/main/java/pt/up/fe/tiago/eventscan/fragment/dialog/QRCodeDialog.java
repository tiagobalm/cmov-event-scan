package pt.up.fe.tiago.eventscan.fragment.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.fragment.cafeteria.CafeteriaFragment;
import pt.up.fe.tiago.eventscan.utils.UpdateOnDismiss;

@SuppressLint("ValidFragment")
public class QRCodeDialog extends AppCompatDialogFragment {

    private Bitmap QRCode;
    private UpdateOnDismiss updateOnDismiss;

    @SuppressLint("ValidFragment")
    public QRCodeDialog(UpdateOnDismiss updateOnDismiss, Bitmap QRCode) {
        this.updateOnDismiss = updateOnDismiss;
        this.QRCode = QRCode;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        @SuppressLint("InflateParams") View view = LayoutInflater.from(getContext()).inflate(R.layout.qr_code_layout, null);
        ImageView QRCodeImage = view.findViewById(R.id.qr_code_image);
        QRCodeImage.setImageBitmap(QRCode);

        builder.setView(view);

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        updateOnDismiss.updateVouchers();
        super.onDismiss(dialog);
    }
}

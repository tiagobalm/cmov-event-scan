package pt.up.fe.tiago.eventscan.room.entity;

import java.util.Date;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "EventSelected")
public class EventSelected {

    @PrimaryKey
    private Integer uuid;
    private String name;
    private Date date;
    private Double price;
    private String imageURL;
    private Integer numberOfTickets;

    public EventSelected(Integer uuid, String name, Date date, Double price, String imageURL, Integer numberOfTickets) {
        this.uuid = uuid;
        this.name = name;
        this.date = date;
        this.price = price;
        this.imageURL = imageURL;
        this.numberOfTickets = numberOfTickets;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Integer getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(Integer numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }
}

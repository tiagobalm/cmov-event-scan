package pt.up.fe.tiago.eventscan.initializer;

import android.app.Application;

import pt.up.fe.tiago.eventscan.dependencyinjection.ApplicationComponent;
import pt.up.fe.tiago.eventscan.dependencyinjection.DaggerApplicationComponent;
import pt.up.fe.tiago.eventscan.dependencyinjection.NetworksModule;
import pt.up.fe.tiago.eventscan.utils.Constants;

public class EventScanApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                                .networksModule(new NetworksModule(Constants.URL))
                                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }
}

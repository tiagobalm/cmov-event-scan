package pt.up.fe.tiago.eventscan.fragment.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Ticket;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.EventRepository;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.adapter.ProfilePastEventsAdapter;
import pt.up.fe.tiago.eventscan.webservice.dto.TicketDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastEventsFragment extends Fragment {

    @Inject
    EventRepository eventRepository;

    private ProfilePastEventsAdapter mAdapter;
    private int currentIndex = 0;
    private int sizePagination = 6;
    private List<Event> events = new ArrayList<>();
    private List<Ticket> tickets = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);

        return inflater.inflate(R.layout.profile_previous_shows, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        NestedScrollView nestedScrollView = Objects.requireNonNull(getActivity()).findViewById(android.R.id.content).findViewById(R.id.profile_nested_scroll_view);

        RecyclerView mRecyclerView = view.findViewById(R.id.used_tickets_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));

        mAdapter = new ProfilePastEventsAdapter(new ArrayList<>(), new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
        eventRepository.getPastEvents(UUID.fromString(sharedPreferences.getString("UUID", ""))).enqueue(new Callback<List<TicketDTO>>() {
            @Override
            public void onResponse(Call<List<TicketDTO>> call, Response<List<TicketDTO>> response) {

                List<TicketDTO> unusedTickets = response.body();

                for(TicketDTO ticketDTO : Objects.requireNonNull(unusedTickets)) {
                    events.add(ticketDTO.getEvent());
                    tickets.add(ticketDTO.getTicket());
                }

                List<Event> firstEvents;
                List<Ticket> firstTickets;

                if(events.size() < sizePagination) {
                    firstEvents = events.subList(currentIndex, events.size());
                    firstTickets = tickets.subList(currentIndex, tickets.size());
                    currentIndex = events.size();
                } else {
                    firstEvents = events.subList(currentIndex, sizePagination);
                    firstTickets = tickets.subList(currentIndex, sizePagination);
                    currentIndex += 6;
                }

                mAdapter.addTicketsInformation(firstEvents, firstTickets);

                nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            List<Event> restEvents;
                            List<Ticket> restTickets;

                            if(currentIndex != events.size()) {
                                if ((currentIndex + sizePagination) < events.size()) {
                                    restEvents = events.subList(currentIndex, currentIndex + sizePagination);
                                    restTickets = tickets.subList(currentIndex, currentIndex + sizePagination);
                                    currentIndex += sizePagination;
                                } else {
                                    restEvents = events.subList(currentIndex, events.size());
                                    restTickets = tickets.subList(currentIndex, tickets.size());
                                    currentIndex = events.size();
                                }

                                mAdapter.addTicketsInformation(restEvents, restTickets);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<List<TicketDTO>> call, Throwable t) {}
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        currentIndex = 0;
        super.onPause();
    }
}


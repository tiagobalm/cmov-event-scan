package pt.up.fe.tiago.eventscan.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Product")
public class Product {

    @PrimaryKey
    private Integer uuid;
    private String name;
    private Double price;

    protected Product() {}

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}

package pt.up.fe.tiago.eventscan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import java.util.Objects;

import javax.inject.Inject;

import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.CustomerRepository;

public class RegisterActivity extends AppCompatActivity {

    @Inject
    CustomerRepository customerRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        ((EventScanApplication)getApplication()).getApplicationComponent().inject(this);

        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);

        if(sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), "") != null
                && !Objects.requireNonNull(sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), "")).equals("")) {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
        }
    }
}

package pt.up.fe.tiago.eventscan.dependencyinjection;

import javax.inject.Singleton;

import dagger.Component;
import pt.up.fe.tiago.eventscan.MainActivity;
import pt.up.fe.tiago.eventscan.RegisterActivity;
import pt.up.fe.tiago.eventscan.fragment.cafeteria.CafeteriaFragment;
import pt.up.fe.tiago.eventscan.fragment.events.EventsFragment;
import pt.up.fe.tiago.eventscan.fragment.profile.PastEventsFragment;
import pt.up.fe.tiago.eventscan.fragment.profile.PreviousCafeteriaFragment;
import pt.up.fe.tiago.eventscan.fragment.profile.UpcomingEventsFragment;
import pt.up.fe.tiago.eventscan.fragment.start.RegisterFirstFragment;
import pt.up.fe.tiago.eventscan.fragment.start.RegisterSecondFragment;
import pt.up.fe.tiago.eventscan.fragment.vouchers.VouchersFragment;
import pt.up.fe.tiago.eventscan.utils.adapter.ShoppingCartAdapter;

@Singleton
@Component(modules = {NetworksModule.class})
public interface ApplicationComponent {
    void inject(RegisterActivity activity);
    void inject(MainActivity mainActivity);
    void inject(RegisterFirstFragment registerFirstFragment);
    void inject(RegisterSecondFragment registerSecondFragment);
    void inject(EventsFragment eventsFragment);
    void inject(CafeteriaFragment cafeteriaFragment);
    void inject(UpcomingEventsFragment upcomingEventsFragment);
    void inject(ShoppingCartAdapter shoppingCartAdapter);
    void inject(PastEventsFragment pastEventsFragment);
    void inject(PreviousCafeteriaFragment previousCafeteriaFragment);
    void inject(VouchersFragment vouchersFragment);
}

package pt.up.fe.tiago.eventscan.utils.adapter;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Product;
import pt.up.fe.tiago.eventscan.fragment.cafeteria.CafeteriaFragment;
import pt.up.fe.tiago.eventscan.room.entity.ProductSelected;
import pt.up.fe.tiago.eventscan.room.viewmodel.ProductSelectedViewModel;
import pt.up.fe.tiago.eventscan.utils.UtilsFunctions;

public class CafeteriaAdapter extends RecyclerView.Adapter<CafeteriaAdapter.ProductHolder> {
    private List<Product> products;
    private CafeteriaFragment cafeteriaFragment;
    private HashMap<Integer, Product> selectedProducts;
    private HashMap<Integer, Integer> numberProducts;
    private FloatingActionButton QRCodeButton;
    private List<Product> addedProducts;
    private ProductSelectedViewModel productViewModel;
    private Boolean productChangedRoom;

    static class ProductHolder extends RecyclerView.ViewHolder {
        TextView productName;
        TextView productPrice;
        EditText numberProduct;
        FloatingActionButton addRemoveButton;
        Boolean addToggle;
        Boolean loaded;

        ProductHolder(View v) {
            super(v);
            this.productName = v.findViewById(R.id.product_name);
            this.productPrice = v.findViewById(R.id.product_price);
            this.numberProduct = v.findViewById(R.id.number_of_products);
            this.addRemoveButton = v.findViewById(R.id.add_product);
            this.addToggle = true;
            this.loaded = false;
        }
    }

    @SuppressLint("UseSparseArrays")
    public CafeteriaAdapter(List<Product> products, CafeteriaFragment cafeteriaFragment, FloatingActionButton QRCodeButton) {
        this.products = products;
        this.cafeteriaFragment = cafeteriaFragment;
        this.selectedProducts = new HashMap<>();
        this.numberProducts = new HashMap<>();
        this.QRCodeButton = QRCodeButton;
        this.addedProducts = new ArrayList<>();
        this.productChangedRoom = false;

        productViewModel = ViewModelProviders.of(cafeteriaFragment).get(ProductSelectedViewModel.class);
    }

    @NonNull
    @Override
    public CafeteriaAdapter.ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_cafeteria_card_item, parent, false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {

        holder.productName.setText(products.get(position).getName());
        holder.productPrice.setText(String.valueOf("Price: " + String.valueOf(products.get(position).getPrice()) + "$"));

        productViewModel.getProductByUUID(products.get(position).getUuid()).observe(cafeteriaFragment, product -> {

            if(!holder.loaded) {
                if (product != null && addedProducts.indexOf(products.get(position)) == -1) {
                    holder.addToggle = false;
                    UtilsFunctions.rotateToClose(holder.addRemoveButton);
                    holder.numberProduct.setText(String.valueOf(product.getNumberOfProducts()));
                    holder.numberProduct.setVisibility(View.VISIBLE);
                    setTextWatcherForEditText(holder, position);

                    selectedProducts.put(position, products.get(position));
                    numberProducts.put(position, product.getNumberOfProducts());

                    if (addedProducts.size() == 0)
                        UtilsFunctions.showPayButton(cafeteriaFragment, QRCodeButton);
                    addedProducts.add(products.get(position));
                }

                holder.addRemoveButton.setOnClickListener(button -> {
                    holder.addToggle = !holder.addToggle;
                    toggleQRCodeButton(holder, position);
                    setTextWatcherForEditText(holder, position);
                });

                holder.loaded = true;
            }
        });
    }

    public void addProducts(List<Product> products) {
        for(int i = 0; i < products.size(); i++) {
            this.products.add(products.get(i));
            notifyItemInserted(this.products.indexOf(products.get(i)));
        }
    }

    private void setTextWatcherForEditText(ProductHolder holder, int position) {

        holder.numberProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    productChangedRoom = false;
                    numberProducts.put(position, Integer.parseInt(s.toString()));

                    productViewModel.getProductByUUID(products.get(position).getUuid()).observe(cafeteriaFragment, product -> {
                        if(!productChangedRoom && product != null) {
                            product.setNumberOfProducts(Integer.parseInt(s.toString()));
                            productViewModel.update(product);
                            productChangedRoom = true;
                        }
                    });
                }
            }
        });
    }

    private void toggleQRCodeButton(ProductHolder holder, int position) {

        if(holder.addToggle) {

            UtilsFunctions.rotateToAdd(holder.addRemoveButton);
            holder.numberProduct.setVisibility(View.GONE);
            productViewModel.deleteByUUID(products.get(position).getUuid());

            selectedProducts.remove(position);
            numberProducts.remove(position);

            addedProducts.remove(products.get(position));
            if(addedProducts.size() == 0) UtilsFunctions.hidePayButton(cafeteriaFragment, QRCodeButton);

        } else {

            UtilsFunctions.rotateToClose(holder.addRemoveButton);

            Product product = products.get(position);
            productViewModel.insert(new ProductSelected(product.getUuid(), product.getName(), product.getPrice(), 1));

            holder.numberProduct.setText("1");
            holder.numberProduct.setVisibility(View.VISIBLE);

            selectedProducts.put(position, products.get(position));
            numberProducts.put(position, 1);

            if(addedProducts.size() == 0) UtilsFunctions.showPayButton(cafeteriaFragment, QRCodeButton);
            addedProducts.add(product);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public HashMap<Integer, Product> getSelectedProducts() { return selectedProducts; }

    public HashMap<Integer, Integer> getNumberProducts() { return numberProducts; }
}


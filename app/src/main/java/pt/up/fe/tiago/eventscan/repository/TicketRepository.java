package pt.up.fe.tiago.eventscan.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import pt.up.fe.tiago.eventscan.webservice.Webservice;
import pt.up.fe.tiago.eventscan.webservice.dto.CreateOrderDTO;
import retrofit2.Call;

@Singleton
public class TicketRepository {

    private final Webservice webservice;

    @Inject
    TicketRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<String> createOrder(CreateOrderDTO createOrderDTO) { return webservice.createOrder(createOrderDTO); }
}

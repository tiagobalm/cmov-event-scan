package pt.up.fe.tiago.eventscan.repository;

import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Singleton;


import pt.up.fe.tiago.eventscan.webservice.Webservice;
import pt.up.fe.tiago.eventscan.webservice.dto.CafeteriaOrderResultDTO;
import retrofit2.Call;

import pt.up.fe.tiago.eventscan.domain.Product;


@Singleton
public class CafeteriaRepository {

    private final Webservice webservice;

    @Inject
    CafeteriaRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<List<Product>> getAllProducts() { return webservice.getAllProducts();}

    public Call<List<CafeteriaOrderResultDTO>> getAllPastOrders(UUID uuid) { return webservice.getAllPastOrders(uuid);}

}

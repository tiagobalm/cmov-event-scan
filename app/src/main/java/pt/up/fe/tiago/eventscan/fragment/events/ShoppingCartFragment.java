package pt.up.fe.tiago.eventscan.fragment.events;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.room.viewmodel.EventSelectedViewModel;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.adapter.ShoppingCartAdapter;

public class ShoppingCartFragment extends Fragment {
    private EventSelectedViewModel eventSelectedViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        eventSelectedViewModel = ViewModelProviders.of(this).get(EventSelectedViewModel.class);
        return inflater.inflate(R.layout.shopping_cart_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView mRecyclerView = view.findViewById(R.id.profile_cafeteria_recycleview);

        FloatingActionButton payButton = view.findViewById(R.id.pay_credit_card);
        ImageView goBack = view.findViewById(R.id.shopping_back);
        goBack.setOnClickListener(v -> Navigation.findNavController(view).navigateUp());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));
        ShoppingCartAdapter mAdapter = new ShoppingCartAdapter(new ArrayList<>(), this, payButton);
        mRecyclerView.setAdapter(mAdapter);

        eventSelectedViewModel.getAllEvents().observe(this, mAdapter::addEvents);

        super.onViewCreated(view, savedInstanceState);
    }

}

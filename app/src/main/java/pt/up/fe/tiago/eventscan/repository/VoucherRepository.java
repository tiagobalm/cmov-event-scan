package pt.up.fe.tiago.eventscan.repository;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import pt.up.fe.tiago.eventscan.domain.Voucher;
import pt.up.fe.tiago.eventscan.webservice.Webservice;
import retrofit2.Call;


@Singleton
public class VoucherRepository {

    private final Webservice webservice;

    @Inject
    VoucherRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<List<Voucher>> getAllUnusedVouchers(UUID uuid) { return webservice.getAllUnusedVouchers(uuid); }

}

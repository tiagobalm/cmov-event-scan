package pt.up.fe.tiago.eventscan.utils;

import android.annotation.SuppressLint;
import java.util.HashMap;

public class Constants {
    public static String URL = "http://10.227.151.23:8080/";
    public static Integer QR_CODE_DIMENSIONS = 800;
    public static Integer KEY_SIZE = 512;
    public static String SIGN_ALGORITHM = "SHA256WithRSA";
    public static String SUCCESS_STRING = "SUCCESS";
    public static String SELECT_A_VOUCHER = "SELECT A VOUCHER";
    public static String POPCORN = "POPCORN";
    public static String COFFEE = "COFFEE";
    public static String DISCOUNT = "FIVEPERCENT";
    public static HashMap<Integer, String> positionToVoucher = createMap();
    private static HashMap<Integer, String> createMap() {
        @SuppressLint("UseSparseArrays") HashMap<Integer,String> myMap = new HashMap<>();
        myMap.put(0, SELECT_A_VOUCHER);
        myMap.put(1, POPCORN);
        myMap.put(2, COFFEE);
        myMap.put(3, DISCOUNT);
        return myMap;
    }
}
package pt.up.fe.tiago.eventscan.room.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import pt.up.fe.tiago.eventscan.room.entity.Product;

@Dao
public interface ProductDao {

    @Insert
    void insert(Product eventSelected);

    @Update
    void update(Product eventSelected);

    @Delete
    void delete(Product eventSelected);

    @Query("SELECT * FROM Product")
    LiveData<List<Product>> getAllProducts();

    @Query("SELECT * FROM Product WHERE uuid = :uuid")
    LiveData<Product> getProductByUUID(Integer uuid);

    @Query("DELETE FROM Product")
    void deleteAllProducts();

    @Query("DELETE FROM Product WHERE uuid = :uuid")
    void deleteByUUID(Integer uuid);
}

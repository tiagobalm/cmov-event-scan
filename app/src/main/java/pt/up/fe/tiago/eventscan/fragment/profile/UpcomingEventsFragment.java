package pt.up.fe.tiago.eventscan.fragment.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Ticket;
import pt.up.fe.tiago.eventscan.fragment.dialog.QRCodeDialog;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.EventRepository;
import pt.up.fe.tiago.eventscan.utils.Constants;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.UpdateOnDismiss;
import pt.up.fe.tiago.eventscan.utils.adapter.ProfileUpcomingEventsAdapter;
import pt.up.fe.tiago.eventscan.webservice.dto.TicketDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpcomingEventsFragment extends Fragment implements UpdateOnDismiss {

    @Inject
    EventRepository eventRepository;

    private ProfileUpcomingEventsAdapter mAdapter;
    private int currentIndex = 0;
    private int sizePagination = 6;
    private List<Event> events;
    private List<Ticket> tickets;
    private UpdateOnDismiss updateOnDismiss;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);
        updateOnDismiss = this;

        return inflater.inflate(R.layout.profile_notused_tickets, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        events = new ArrayList<>();
        tickets = new ArrayList<>();
        NestedScrollView nestedScrollView = Objects.requireNonNull(getActivity()).findViewById(android.R.id.content).findViewById(R.id.profile_nested_scroll_view);

        RecyclerView mRecyclerView = view.findViewById(R.id.not_usedTickets_recycler_view);
        FloatingActionButton QRCode = Objects.requireNonNull(getActivity()).findViewById(R.id.ticket_QR_code);
        QRCode.hide();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));

        mAdapter = new ProfileUpcomingEventsAdapter(new ArrayList<>(), new ArrayList<>(),this, QRCode);
        mRecyclerView.setAdapter(mAdapter);

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
        eventRepository.getUpComingEvents(UUID.fromString(sharedPreferences.getString("UUID", ""))).enqueue(new Callback<List<TicketDTO>>() {
            @Override
            public void onResponse(Call<List<TicketDTO>> call, Response<List<TicketDTO>> response) {

                List<TicketDTO> unusedTickets = response.body();

                for(TicketDTO ticketDTO : Objects.requireNonNull(unusedTickets)) {
                    events.add(ticketDTO.getEvent());
                    tickets.add(ticketDTO.getTicket());
                }

                List<Event> firstEvents;
                List<Ticket> firstTickets;

                if(events.size() < sizePagination) {
                    firstEvents = events.subList(currentIndex, events.size());
                    firstTickets = tickets.subList(currentIndex, tickets.size());
                    currentIndex = events.size();
                } else {
                    firstEvents = events.subList(currentIndex, sizePagination);
                    firstTickets = tickets.subList(currentIndex, sizePagination);
                    currentIndex += sizePagination;
                }

                mAdapter.addTicketsInformation(firstEvents, firstTickets);

                nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            List<Event> restEvents;
                            List<Ticket> restTickets;

                            if(currentIndex != events.size()) {
                                if ((currentIndex + sizePagination) < events.size()) {
                                    restEvents = events.subList(currentIndex, currentIndex + sizePagination);
                                    restTickets = tickets.subList(currentIndex, currentIndex + sizePagination);
                                    currentIndex += sizePagination;
                                } else {
                                    restEvents = events.subList(currentIndex, events.size());
                                    restTickets = tickets.subList(currentIndex, tickets.size());
                                    currentIndex = events.size();
                                }

                                mAdapter.addTicketsInformation(restEvents, restTickets);
                            }
                        }
                    }
                });

                QRCode.setOnClickListener(v -> {
                    List<Ticket> selectedTickets = mAdapter.getSelectedTickets();

                    SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
                    String customerUUID = sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), "");

                    JSONObject jsonForQRCode = new JSONObject();
                    JSONArray selectedTicketsJSON = new JSONArray();
                    try {
                        jsonForQRCode.put("customeruuid", customerUUID);

                        for(Ticket ticket : selectedTickets) {

                            selectedTicketsJSON.put(ticket.getUuid());
                        }

                        jsonForQRCode.put("tickets", selectedTicketsJSON);

                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        BitMatrix bitMatrix = multiFormatWriter.encode(jsonForQRCode.toString(), BarcodeFormat.QR_CODE, Constants.QR_CODE_DIMENSIONS, Constants.QR_CODE_DIMENSIONS);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        new QRCodeDialog(updateOnDismiss, barcodeEncoder.createBitmap(bitMatrix)).show(Objects.requireNonNull(getFragmentManager()), "QR Code Dialog");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                });
            }

            @Override
            public void onFailure(Call<List<TicketDTO>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        currentIndex = 0;
        super.onPause();
    }

    @Override
    public void updateVouchers() {
        FragmentTransaction ft = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}

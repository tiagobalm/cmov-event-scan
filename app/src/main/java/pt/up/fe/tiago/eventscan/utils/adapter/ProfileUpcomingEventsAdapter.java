package pt.up.fe.tiago.eventscan.utils.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Ticket;
import pt.up.fe.tiago.eventscan.fragment.profile.UpcomingEventsFragment;
import pt.up.fe.tiago.eventscan.utils.UtilsFunctions;

public class ProfileUpcomingEventsAdapter extends RecyclerView.Adapter<ProfileUpcomingEventsAdapter.UpcomingHolder> {
    private List<Event> events;
    private List<Ticket> tickets;
    private FloatingActionButton QRCode;
    private List<Ticket> addedTickets;
    private UpcomingEventsFragment upcomingEventsFragment;

    static class UpcomingHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventPrice;
        TextView eventDate;
        TextView eventSeat;
        ImageView eventPhoto;
        EditText numberTickets;
        FloatingActionButton addValidate;
        Boolean addToggle;

        UpcomingHolder(View v) {
            super(v);
            eventName = v.findViewById(R.id.event_name);
            eventPrice = v.findViewById(R.id.event_price_input);
            eventDate = v.findViewById(R.id.event_date_input);
            eventPhoto = v.findViewById(R.id.event_photo);
            eventSeat = v.findViewById(R.id.event_seat_input);
            numberTickets = v.findViewById(R.id.number_tickets);
            addValidate = v.findViewById(R.id.add_validate);
            addToggle = true;
        }
    }

    public ProfileUpcomingEventsAdapter(List<Event> events, List<Ticket> tickets, UpcomingEventsFragment upcomingEventsFragment, FloatingActionButton QRCode) {
        this.events = events;
        this.tickets = tickets;
        this.QRCode = QRCode;
        this.upcomingEventsFragment = upcomingEventsFragment;
        this.addedTickets = new ArrayList<>();
    }

    @NonNull
    @Override
    public ProfileUpcomingEventsAdapter.UpcomingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_card_item, parent, false);
        return new UpcomingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UpcomingHolder holder, int position) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        holder.eventName.setText(events.get(position).getName());
        holder.eventDate.setText(dateFormat.format(events.get(position).getDate()));
        Glide.with(Objects.requireNonNull(upcomingEventsFragment.getContext())).load(Uri.parse(events.get(position).getImageURL())).into(holder.eventPhoto);
        holder.eventPrice.setText(String.valueOf(events.get(position).getPrice().toString() + "$"));
        holder.eventSeat.setText(String.valueOf(tickets.get(position).getSeat()));

        holder.addValidate.setOnClickListener(button -> {
            if(addedTickets.size() < 4 || (addedTickets.size() == 4 && !holder.addToggle)) {
                holder.addToggle = !holder.addToggle;
                toggleValidate(holder, position);
            }
        });
    }

    private void toggleValidate(UpcomingHolder holder, int position) {

        if(holder.addToggle) {

            RotateAnimation rotateAnimation = new RotateAnimation(405, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(400);
            rotateAnimation.setFillAfter(true);
            rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
            holder.addValidate.setAnimation(rotateAnimation);
            holder.addValidate.startAnimation(rotateAnimation);

            addedTickets.remove(tickets.get(position));

            if(addedTickets.size() == 0)
                UtilsFunctions.hidePayButton(upcomingEventsFragment, QRCode);

        } else {

            RotateAnimation rotateAnimation = new RotateAnimation(0, 405, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(400);
            rotateAnimation.setFillAfter(true);
            rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
            holder.addValidate.setAnimation(rotateAnimation);
            holder.addValidate.startAnimation(rotateAnimation);

            if(addedTickets.size() == 0)
                UtilsFunctions.showPayButton(upcomingEventsFragment, QRCode);

            addedTickets.add(tickets.get(position));
        }
    }

    public void addTicketsInformation(List<Event> events, List<Ticket> tickets) {
        for(int i = 0; i < events.size(); i++) {
            this.events.add(events.get(i));
            this.tickets.add(tickets.get(i));
            notifyItemInserted(this.events.indexOf(events.get(i)));
        }
    }

    public List<Ticket> getSelectedTickets() { return addedTickets; }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}

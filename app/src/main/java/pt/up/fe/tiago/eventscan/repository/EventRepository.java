package pt.up.fe.tiago.eventscan.repository;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.webservice.Webservice;
import pt.up.fe.tiago.eventscan.webservice.dto.TicketDTO;
import retrofit2.Call;

@Singleton
public class EventRepository {

    private final Webservice webservice;

    @Inject
    EventRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<List<Event>> getAllEvents() { return webservice.getAllEvents(); }

    public Call<List<TicketDTO>> getUpComingEvents(UUID uuid) { return webservice.getUpComingEvents(uuid); }

    public Call<List<TicketDTO>> getPastEvents(UUID uuid) { return webservice.getPastEvents(uuid); }


}

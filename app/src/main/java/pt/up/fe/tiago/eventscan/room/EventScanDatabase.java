package pt.up.fe.tiago.eventscan.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import pt.up.fe.tiago.eventscan.room.dao.EventDao;
import pt.up.fe.tiago.eventscan.room.dao.EventSelectedDao;
import pt.up.fe.tiago.eventscan.room.dao.ProductDao;
import pt.up.fe.tiago.eventscan.room.dao.ProductSelectedDao;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;
import pt.up.fe.tiago.eventscan.room.entity.ProductSelected;
import pt.up.fe.tiago.eventscan.utils.Converters;

@Database(entities = {EventSelected.class, ProductSelected.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class EventScanDatabase extends RoomDatabase {

    private static EventScanDatabase instance;

    public abstract EventSelectedDao eventSelectedDao();
    public abstract ProductSelectedDao productSelectedDao();
    public abstract EventDao eventDao();
    public abstract ProductDao productDao();

    public static synchronized EventScanDatabase getInstance(Context context) {

        if(instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    EventScanDatabase.class, "eventscandatabase")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }
}

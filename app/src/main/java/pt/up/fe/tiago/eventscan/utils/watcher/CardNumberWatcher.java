package pt.up.fe.tiago.eventscan.utils.watcher;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

public class CardNumberWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() > 0 && (editable.length() % 5) == 0) {
            char c = editable.charAt(editable.length() - 1);
            if (' ' == c) {
                editable.delete(editable.length() - 1, editable.length());
            }
        }

        if (editable.length() > 0 && (editable.length() % 5) == 0) {
            char c = editable.charAt(editable.length() - 1);
            if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf(" ")).length <= 4) {
                editable.insert(editable.length() - 1, String.valueOf(" "));
            }
        }
    }
}

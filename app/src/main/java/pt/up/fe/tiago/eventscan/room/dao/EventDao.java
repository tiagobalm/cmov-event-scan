package pt.up.fe.tiago.eventscan.room.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import pt.up.fe.tiago.eventscan.room.entity.Event;

@Dao
public interface EventDao {

    @Insert
    void insert(Event eventSelected);

    @Update
    void update(Event eventSelected);

    @Delete
    void delete(Event eventSelected);

    @Query("SELECT * FROM Event")
    LiveData<List<Event>> getAllEvents();

    @Query("SELECT * FROM Event WHERE uuid = :uuid")
    LiveData<Event> getEventByUUID(Integer uuid);

    @Query("DELETE FROM Event")
    void deleteAllEvents();

    @Query("DELETE FROM Event WHERE uuid = :uuid")
    void deleteByUUID(Integer uuid);
}

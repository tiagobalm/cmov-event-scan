package pt.up.fe.tiago.eventscan.room.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;

@Dao
public interface EventSelectedDao {

    @Insert
    void insert(EventSelected eventSelected);

    @Update
    void update(EventSelected eventSelected);

    @Delete
    void delete(EventSelected eventSelected);

    @Query("SELECT * FROM EventSelected")
    LiveData<List<EventSelected>> getAllEvents();

    @Query("SELECT * FROM EventSelected WHERE uuid = :uuid")
    LiveData<EventSelected> getEventByUUID(Integer uuid);

    @Query("DELETE FROM EventSelected")
    void deleteAllEvents();

    @Query("DELETE FROM EventSelected WHERE uuid = :uuid")
    void deleteByUUID(Integer uuid);
}

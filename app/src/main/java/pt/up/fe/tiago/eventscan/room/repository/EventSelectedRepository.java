package pt.up.fe.tiago.eventscan.room.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.EventScanDatabase;
import pt.up.fe.tiago.eventscan.room.dao.EventSelectedDao;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;

public class EventSelectedRepository {
    private EventSelectedDao eventSelectedDao;
    private LiveData<List<EventSelected>> allEvents;

    public EventSelectedRepository(Application application) {
        EventScanDatabase eventScanDatabase = EventScanDatabase.getInstance(application);
        eventSelectedDao = eventScanDatabase.eventSelectedDao();
        allEvents = eventSelectedDao.getAllEvents();
    }

    public void insert(EventSelected eventSelected) {
        new InsertEvent(eventSelectedDao).execute(eventSelected);
    }

    public void update(EventSelected eventSelected) {
        new UpdateEvent(eventSelectedDao).execute(eventSelected);
    }

    public void delete(EventSelected eventSelected) {
        new DeleteEvent(eventSelectedDao).execute(eventSelected);
    }

    public LiveData<List<EventSelected>> getAllEvents() {
        return allEvents;
    }

    public LiveData<EventSelected> getEventByUUID(Integer uuid) { return eventSelectedDao.getEventByUUID(uuid); }

    public void deleteAllEvents() {
        new DeleteAllEvents(eventSelectedDao).execute();
    }

    public void deleteByUUID(Integer uuid) {
        new DeleteByUUID(eventSelectedDao).execute(uuid);
    }

    private static class InsertEvent extends AsyncTask<EventSelected, Void, Void> {
        private EventSelectedDao eventSelectedDao;

        private InsertEvent(EventSelectedDao eventSelectedDao) {
            this.eventSelectedDao = eventSelectedDao;
        }

        @Override
        protected Void doInBackground(EventSelected... eventSelecteds) {
            eventSelectedDao.insert(eventSelecteds[0]);
            return null;
        }
    }

    private static class UpdateEvent extends AsyncTask<EventSelected, Void, Void> {
        private EventSelectedDao eventSelectedDao;

        private UpdateEvent(EventSelectedDao eventSelectedDao) {
            this.eventSelectedDao = eventSelectedDao;
        }

        @Override
        protected Void doInBackground(EventSelected... eventSelecteds) {
            eventSelectedDao.update(eventSelecteds[0]);
            return null;
        }
    }

    private static class DeleteEvent extends AsyncTask<EventSelected, Void, Void> {
        private EventSelectedDao eventSelectedDao;

        private DeleteEvent(EventSelectedDao eventSelectedDao) {
            this.eventSelectedDao = eventSelectedDao;
        }

        @Override
        protected Void doInBackground(EventSelected... eventSelecteds) {
            eventSelectedDao.delete(eventSelecteds[0]);
            return null;
        }
    }

    private static class DeleteAllEvents extends AsyncTask<Void, Void, Void> {
        private EventSelectedDao eventSelectedDao;

        private DeleteAllEvents(EventSelectedDao eventSelectedDao) {
            this.eventSelectedDao = eventSelectedDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            eventSelectedDao.deleteAllEvents();
            return null;
        }
    }

    private static class DeleteByUUID extends AsyncTask<Integer, Void, Void> {
        private EventSelectedDao eventSelectedDao;

        private DeleteByUUID(EventSelectedDao eventSelectedDao) {
            this.eventSelectedDao = eventSelectedDao;
        }

        @Override
        protected Void doInBackground(Integer... uuid) {
            eventSelectedDao.deleteByUUID(uuid[0]);
            return null;
        }
    }
}

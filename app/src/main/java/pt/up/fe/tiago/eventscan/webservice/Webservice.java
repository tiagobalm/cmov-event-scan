package pt.up.fe.tiago.eventscan.webservice;

import java.util.List;
import java.util.UUID;

import pt.up.fe.tiago.eventscan.domain.Customer;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Product;
import pt.up.fe.tiago.eventscan.domain.Voucher;
import pt.up.fe.tiago.eventscan.webservice.dto.CafeteriaOrderResultDTO;
import pt.up.fe.tiago.eventscan.webservice.dto.CreateOrderDTO;
import pt.up.fe.tiago.eventscan.webservice.dto.TicketDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Webservice {

    @POST("customer/create")
    Call<UUID> createCustomer(@Body Customer costumer);

    @GET("events")
    Call<List<Event>> getAllEvents();

    @GET("products")
    Call<List<Product>> getAllProducts();

    @GET("tickets/notused/{uuid}")
    Call<List<TicketDTO>> getUpComingEvents(@Path("uuid") UUID uuid);

    @POST("tickets/process")
    Call<String> createOrder(@Body CreateOrderDTO createOrderDTO);

    @GET("tickets/used/{uuid}")
    Call<List<TicketDTO>> getPastEvents(@Path("uuid") UUID uuid);

    @GET("invoice/past/{uuid}")
    Call<List<CafeteriaOrderResultDTO>> getAllPastOrders(@Path("uuid") UUID uuid);

    @GET("/vouchers/unused/{uuid}")
    Call<List<Voucher>> getAllUnusedVouchers(@Path("uuid") UUID uuid);
}
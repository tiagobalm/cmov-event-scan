package pt.up.fe.tiago.eventscan.fragment.cafeteria;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Product;
import pt.up.fe.tiago.eventscan.domain.Voucher;
import pt.up.fe.tiago.eventscan.fragment.dialog.QRCodeDialog;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.CafeteriaRepository;
import pt.up.fe.tiago.eventscan.repository.VoucherRepository;
import pt.up.fe.tiago.eventscan.room.viewmodel.ProductSelectedViewModel;
import pt.up.fe.tiago.eventscan.utils.Constants;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.UpdateOnDismiss;
import pt.up.fe.tiago.eventscan.utils.adapter.CafeteriaAdapter;
import pt.up.fe.tiago.eventscan.webservice.dto.CafeteriaOrderDTO;
import pt.up.fe.tiago.eventscan.webservice.dto.CreateOrderDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CafeteriaFragment extends Fragment implements UpdateOnDismiss {

    @Inject
    CafeteriaRepository cafeteriaRepository;

    @Inject
    VoucherRepository voucherRepository;

    @Inject
    Gson gson;

    private List<Product> products;
    private ProgressBar progressBar;
    private int sizePagination = 6;
    private int currentIndex = 0;
    private NestedScrollView nestedScrollView;
    private FloatingActionButton QRCodeButton;
    private RecyclerView mRecyclerView;
    private CafeteriaAdapter mAdapter;
    private ImageView voucherOneImage, voucherTwoImage;
    private TextView voucherOneTitle, voucherTwoTitle;
    private HashMap<String, Integer> numberOfVouchers;
    private List<Voucher> vouchers;
    private Integer voucherOnePosition = 0;
    private Integer voucherTwoPosition = 0;
    private String currentVoucherOne = Constants.SELECT_A_VOUCHER;
    private String currentVoucherTwo= Constants.SELECT_A_VOUCHER;
    private UpdateOnDismiss cafeteriaFragment;
    private ProductSelectedViewModel productViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);

        return inflater.inflate(R.layout.cafeteria_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        products = new ArrayList<>();
        numberOfVouchers = new HashMap<>();
        voucherOnePosition = 0;
        voucherTwoPosition = 0;
        currentVoucherOne = Constants.SELECT_A_VOUCHER;
        currentVoucherTwo= Constants.SELECT_A_VOUCHER;

        progressBar = view.findViewById(R.id.cafeteria_loading);
        nestedScrollView = view.findViewById(R.id.cafeteria_nested_scroll_view);
        mRecyclerView = view.findViewById(R.id.cafeteria_recycler_view);
        CardView voucherOne = view.findViewById(R.id.voucher_one);
        CardView voucherTwo = view.findViewById(R.id.voucher_two);
        voucherOneImage = view.findViewById(R.id.voucher_image_one);
        voucherTwoImage = view.findViewById(R.id.voucher_image_two);
        voucherOneTitle = view.findViewById(R.id.voucher_title_one);
        voucherTwoTitle = view.findViewById(R.id.voucher_title_two);

        QRCodeButton = view.findViewById(R.id.pay_cafeteria);
        QRCodeButton.hide();

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));

        getAvailableVouchers();
        voucherOne.setOnClickListener(v -> setVouchersBehavior(1, voucherOneImage, voucherOneTitle));
        voucherTwo.setOnClickListener(v -> setVouchersBehavior(2, voucherTwoImage, voucherTwoTitle));

        fillRecyclerView();

        productViewModel = ViewModelProviders.of(this).get(ProductSelectedViewModel.class);

        this.cafeteriaFragment = this;
        super.onViewCreated(view, savedInstanceState);
    }

    private void setVouchersBehavior(Integer voucher, ImageView imageView, TextView textView) {
        String currentVoucher;

        if(voucher == 1) {
            voucherOnePosition = (voucherOnePosition + 1) % 4;
            while((currentVoucher = changeResourcesBasedOnPosition(voucherOnePosition, imageView, textView)).equals(""))
                voucherOnePosition = (voucherOnePosition + 1) % 4;

            String previousVoucherOne = currentVoucherOne;
            currentVoucherOne = currentVoucher;

            if(!previousVoucherOne.equals(Constants.SELECT_A_VOUCHER))
                numberOfVouchers.put(previousVoucherOne, numberOfVouchers.get(previousVoucherOne) + 1);
        } else {
            voucherTwoPosition = (voucherTwoPosition + 1) % 4;
            while((currentVoucher = changeResourcesBasedOnPosition(voucherTwoPosition, imageView, textView)).equals(""))
                voucherTwoPosition = (voucherTwoPosition + 1) % 4;

            String previousVoucherTwo = currentVoucherTwo;
            currentVoucherTwo = currentVoucher;

            if(!previousVoucherTwo.equals(Constants.SELECT_A_VOUCHER))
                numberOfVouchers.put(previousVoucherTwo, numberOfVouchers.get(previousVoucherTwo) + 1);
        }
        System.out.println("Set behavior: " + numberOfVouchers);
    }

    private String changeResourcesBasedOnPosition(Integer voucherPosition, ImageView imageView, TextView textView) {

        if(Objects.equals(Constants.positionToVoucher.get(voucherPosition), Constants.SELECT_A_VOUCHER)) {
            imageView.setImageResource(R.drawable.ic_discount_voucher_100dp);
            textView.setText(String.valueOf("Select a voucher."));

            return Constants.SELECT_A_VOUCHER;
        } else if(Objects.equals(Constants.positionToVoucher.get(voucherPosition), Constants.POPCORN)) {
            if(numberOfVouchers.containsKey(Constants.POPCORN) && numberOfVouchers.get(Constants.POPCORN) > 0) {
                imageView.setImageResource(R.drawable.ic_popcorn);
                textView.setText(String.valueOf("Free"));
                numberOfVouchers.put(Constants.POPCORN, numberOfVouchers.get(Constants.POPCORN) - 1);
                return Constants.POPCORN;
            } else
                return "";
        } else if(Objects.equals(Constants.positionToVoucher.get(voucherPosition), Constants.COFFEE)) {
            if(numberOfVouchers.containsKey(Constants.COFFEE) && numberOfVouchers.get(Constants.COFFEE) > 0) {
                imageView.setImageResource(R.drawable.ic_coffee_for_voucher);
                textView.setText(String.valueOf("Free"));
                numberOfVouchers.put(Constants.COFFEE, numberOfVouchers.get(Constants.COFFEE) - 1);
                return Constants.COFFEE;
            } else
                return "";
        } else if(Objects.equals(Constants.positionToVoucher.get(voucherPosition), Constants.DISCOUNT)) {
            if(numberOfVouchers.containsKey(Constants.DISCOUNT) && numberOfVouchers.get(Constants.DISCOUNT) > 0) {
                imageView.setImageResource(R.drawable.ic_five_percent);
                textView.setText(String.valueOf("Discount"));
                numberOfVouchers.put(Constants.DISCOUNT, numberOfVouchers.get(Constants.DISCOUNT) - 1);
                return Constants.DISCOUNT;
            } else
                return "";
        }

        return "";
    }

    private void getAvailableVouchers() {

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
        voucherRepository.getAllUnusedVouchers(UUID.fromString(sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), ""))).enqueue(new Callback<List<Voucher>>() {
            @Override
            public void onResponse(Call<List<Voucher>> call, Response<List<Voucher>> response) {
                vouchers = response.body();
                System.out.println("Vouchers: " + vouchers);

                for(Voucher voucher: Objects.requireNonNull(vouchers)) {

                    if(!numberOfVouchers.containsKey(voucher.getProductCode()))
                        numberOfVouchers.put(voucher.getProductCode(), 1);
                    else
                        numberOfVouchers.put(voucher.getProductCode(), numberOfVouchers.get(voucher.getProductCode()) + 1);
                }

                System.out.println("Get available vouchers: " + numberOfVouchers);
            }

            @Override
            public void onFailure(Call<List<Voucher>> call, Throwable t) { }
        });
    }

    private void fillRecyclerView() {

        mAdapter = new CafeteriaAdapter(new ArrayList<>(), this, QRCodeButton);
        mRecyclerView.setAdapter(mAdapter);

        cafeteriaRepository.getAllProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

               products = response.body();

                List<Product> firstProducts;

                if(Objects.requireNonNull(products).size() < sizePagination) {
                    firstProducts = products.subList(currentIndex, products.size());
                    currentIndex = products.size();
                } else {
                    firstProducts = products.subList(currentIndex, sizePagination);
                    currentIndex += sizePagination;
                }

                mAdapter.addProducts(firstProducts);
                progressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                nestedScrollView.fullScroll(ScrollView.FOCUS_UP);
                nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            List<Product> restProducts;

                            if(currentIndex != products.size()) {
                                if ((currentIndex + sizePagination) < products.size()) {
                                    restProducts = products.subList(currentIndex, currentIndex + sizePagination);
                                    currentIndex += sizePagination;
                                } else {
                                    restProducts = products.subList(currentIndex, products.size());
                                    currentIndex = products.size();
                                }

                                mAdapter.addProducts(restProducts);
                            }
                        }
                    }
                });

                QRCodeButton.setOnClickListener(v -> {
                    HashMap<Integer, Product> products = mAdapter.getSelectedProducts();
                    HashMap<Integer, Integer> numberProducts = mAdapter.getNumberProducts();

                    SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
                    String customerUUID = sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), "");

                    CafeteriaOrderDTO cafeteriaOrderDTO = new CafeteriaOrderDTO(UUID.fromString(customerUUID), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

                    try {

                        for(Integer i : products.keySet()) {
                            cafeteriaOrderDTO.getProducts().add(Objects.requireNonNull(products.get(i)).getUuid());
                            cafeteriaOrderDTO.getNumberProducts().add(numberProducts.get(i));
                        }

                        UUID voucherOne = null, voucherTwo = null;
                        int index = 0;

                        while((voucherOne == null || voucherTwo == null) && index < vouchers.size()) {

                            if(voucherOne == null && vouchers.get(index).getProductCode().equals(currentVoucherOne)) {
                                voucherOne = vouchers.get(index).getUuid();
                                index++;
                                continue;
                            }

                            if(voucherTwo == null && vouchers.get(index).getProductCode().equals(currentVoucherTwo)) {
                                voucherTwo = vouchers.get(index).getUuid();
                                index++;
                                continue;
                            }

                            index++;
                        }

                        cafeteriaOrderDTO.getVouchers().add(voucherOne);
                        cafeteriaOrderDTO.getVouchers().add(voucherTwo);

                        Signature sig = Signature.getInstance(Constants.SIGN_ALGORITHM);
                        byte[] keyModulus = Base64.decode(sharedPreferences.getString(getActivity().getResources().getString(R.string.MODULUS_TAG), ""), Base64.DEFAULT);
                        byte[] keyExponent = Base64.decode(sharedPreferences.getString(getActivity().getResources().getString(R.string.EXPONENT_TAG), ""), Base64.DEFAULT);

                        RSAPrivateKeySpec RSASpec = new RSAPrivateKeySpec(new BigInteger(keyModulus), new BigInteger(keyExponent));
                        KeyFactory factory = KeyFactory.getInstance("RSA");
                        PrivateKey privateKey = factory.generatePrivate(RSASpec);

                        sig.initSign(privateKey);
                        sig.update(gson.toJson(cafeteriaOrderDTO).getBytes(StandardCharsets.UTF_8));
                        byte[] signatureBytes = sig.sign();

                        CreateOrderDTO createOrderDTO = new CreateOrderDTO(signatureBytes, gson.toJson(cafeteriaOrderDTO).getBytes(StandardCharsets.UTF_8));

                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        BitMatrix bitMatrix = multiFormatWriter.encode(gson.toJson(createOrderDTO), BarcodeFormat.QR_CODE, Constants.QR_CODE_DIMENSIONS, Constants.QR_CODE_DIMENSIONS);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        new QRCodeDialog(cafeteriaFragment, barcodeEncoder.createBitmap(bitMatrix)).show(Objects.requireNonNull(getFragmentManager()), "QR Code Dialog");

                    } catch (WriterException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeySpecException e) {
                        e.printStackTrace();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {}
        });
    }

    @Override
    public void onPause() {
        currentIndex = 0;
        super.onPause();
    }

    public void updateVouchers() {
        productViewModel.deleteAllProducts();
        changeResourcesBasedOnPosition(voucherOnePosition, voucherOneImage, voucherOneTitle);
        changeResourcesBasedOnPosition(voucherTwoPosition, voucherTwoImage, voucherTwoTitle);
        FragmentTransaction ft = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}

package pt.up.fe.tiago.eventscan.fragment.events;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.core.view.ViewCompat;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.EventRepository;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.adapter.EventsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsFragment extends Fragment {

    @Inject
    EventRepository eventRepository;

    private int currentIndex = 0;
    private int sizePagination = 6;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);

        return inflater.inflate(R.layout.dashboard_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView mRecyclerView = view.findViewById(R.id.events_recycler_view);
        ProgressBar progressBar = view.findViewById(R.id.events_loading);
        NestedScrollView nestedScrollView = view.findViewById(R.id.dashboard_nested_scroll_view);

        FloatingActionButton payButton = view.findViewById(R.id.pay_tickets);
        payButton.hide();
        payButton.setOnClickListener(v -> Navigation.findNavController(view).navigate(R.id.action_dashboard_to_shoppingCartFragment));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));
        EventsAdapter mAdapter = new EventsAdapter(new ArrayList<>(), this, payButton);
        mRecyclerView.setAdapter(mAdapter);

        eventRepository.getAllEvents().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                List<Event> events = response.body();
                List<Event> firstFourEvents;

                if(Objects.requireNonNull(events).size() < sizePagination) {
                    firstFourEvents = events.subList(currentIndex, events.size());
                    currentIndex = events.size();
                } else {
                    firstFourEvents = events.subList(currentIndex, sizePagination);
                    currentIndex += sizePagination;
                }

                mAdapter.addEvents(firstFourEvents);
                progressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            List<Event> restEvents;

                            if(currentIndex != events.size()) {
                                if ((currentIndex + sizePagination) < events.size()) {
                                    restEvents = events.subList(currentIndex, currentIndex + sizePagination);
                                    currentIndex += sizePagination;
                                } else {
                                    restEvents = events.subList(currentIndex, events.size());
                                    currentIndex = events.size();
                                }

                                mAdapter.addEvents(restEvents);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {}
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        currentIndex = 0;
        super.onPause();
    }
}

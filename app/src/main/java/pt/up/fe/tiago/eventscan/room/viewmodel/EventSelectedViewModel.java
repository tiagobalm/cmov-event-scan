package pt.up.fe.tiago.eventscan.room.viewmodel;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;
import pt.up.fe.tiago.eventscan.room.repository.EventSelectedRepository;

public class EventSelectedViewModel extends AndroidViewModel {
    private EventSelectedRepository eventSelectedRepository;
    private LiveData<List<EventSelected>> allEvents;

    public EventSelectedViewModel(@NonNull Application application) {
        super(application);
        eventSelectedRepository = new EventSelectedRepository(application);
        allEvents = eventSelectedRepository.getAllEvents();
    }

    public void insert(EventSelected eventSelected) { eventSelectedRepository.insert(eventSelected); }
    public void update(EventSelected eventSelected) { eventSelectedRepository.update(eventSelected); }
    public void delete(EventSelected eventSelected) { eventSelectedRepository.delete(eventSelected); }
    public void deleteAllEvents() { eventSelectedRepository.deleteAllEvents(); }
    public void deleteByUUID(Integer uuid) { eventSelectedRepository.deleteByUUID(uuid); }
    public LiveData<List<EventSelected>> getAllEvents() { return allEvents; }
    public LiveData<EventSelected> getEventByUUID(Integer uuid) { return eventSelectedRepository.getEventByUUID(uuid); }
}

package pt.up.fe.tiago.eventscan.utils;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import pt.up.fe.tiago.eventscan.R;

public class UtilsFunctions {

    public static void showErrorMessage(EditText editText){
        editText.setError("This field cannot be empty.");
    }

    public static void showPayButton(Fragment fragment, FloatingActionButton button) {
        button.show();
        button.setClickable(true);
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(fragment.getContext(), R.anim.pay_button_in);
        fadeOutAnimation.setFillAfter(true);
        button.startAnimation(fadeOutAnimation);
    }

    public static void hidePayButton(Fragment fragment, FloatingActionButton button) {
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(fragment.getContext(), R.anim.pay_button_out);
        fadeOutAnimation.setFillAfter(true);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                button.setClickable(false);
            }
        });
        button.startAnimation(fadeOutAnimation);
    }

    public static void rotateToAdd(FloatingActionButton button) {

        RotateAnimation rotateAnimation = new RotateAnimation(405, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(400);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        button.setAnimation(rotateAnimation);
        button.startAnimation(rotateAnimation);

    }

    public static void rotateToClose(FloatingActionButton button) {

        RotateAnimation rotateAnimation = new RotateAnimation(0, 405, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(400);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        button.setAnimation(rotateAnimation);
        button.startAnimation(rotateAnimation);

    }

    public static void fillVoucher(String productCode, ImageView imageView, TextView textView) {
        if(productCode.toLowerCase().contains(Constants.POPCORN.toLowerCase())) {
            imageView.setImageResource(R.drawable.ic_popcorn);
            textView.setText(String.valueOf("Free"));
        } else if(productCode.toLowerCase().contains(Constants.COFFEE.toLowerCase())) {
            imageView.setImageResource(R.drawable.ic_coffee_for_voucher);
            textView.setText(String.valueOf("Free"));
        } else {
            imageView.setImageResource(R.drawable.ic_five_percent);
            textView.setText(String.valueOf("Discount"));
        }
    }
}

package pt.up.fe.tiago.eventscan.webservice.dto;


import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Ticket;

public class TicketDTO {

    private Event event;
    private Ticket ticket;

    public TicketDTO() {}

    public TicketDTO(Event event, Ticket ticket) {
        this.event = event;
        this.ticket = ticket;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket seat) {
        this.ticket = seat;
    }
}


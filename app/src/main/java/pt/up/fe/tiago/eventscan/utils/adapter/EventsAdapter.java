package pt.up.fe.tiago.eventscan.utils.adapter;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.fragment.events.EventsFragment;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;
import pt.up.fe.tiago.eventscan.room.viewmodel.EventSelectedViewModel;
import pt.up.fe.tiago.eventscan.utils.UtilsFunctions;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventHolder> {
    private List<Event> events;
    private EventsFragment eventsFragment;
    private FloatingActionButton payButton;
    private List<Event> addedTickets;
    private EventSelectedViewModel eventSelectedViewModel;

    static class EventHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventPrice;
        TextView eventDate;
        ImageView eventPhoto;
        EditText numberTickets;
        FloatingActionButton addRemoveButton;
        Boolean addToggle;

        EventHolder(View v) {
            super(v);
            eventName = v.findViewById(R.id.event_name);
            eventPrice = v.findViewById(R.id.event_price_input);
            eventDate = v.findViewById(R.id.event_date_input);
            eventPhoto = v.findViewById(R.id.event_photo);
            numberTickets = v.findViewById(R.id.number_tickets);
            addRemoveButton = v.findViewById(R.id.add_remove_button);
            addToggle = true;
        }
    }

    public EventsAdapter(List<Event> events, EventsFragment eventsFragment, FloatingActionButton payButton) {
        this.events = events;
        this.eventsFragment = eventsFragment;
        this.payButton = payButton;
        this.addedTickets = new ArrayList<>();

        eventSelectedViewModel = ViewModelProviders.of(eventsFragment).get(EventSelectedViewModel.class);
    }

    @NonNull
    @Override
    public EventsAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card_item, parent, false);
        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, int position) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        holder.eventName.setText(events.get(position).getName());
        holder.eventDate.setText(dateFormat.format(events.get(position).getDate()));
        Glide.with(Objects.requireNonNull(eventsFragment.getContext())).load(Uri.parse(events.get(position).getImageURL())).into(holder.eventPhoto);
        holder.eventPrice.setText(String.valueOf(events.get(position).getPrice().toString() + "$"));

        eventSelectedViewModel.getEventByUUID(events.get(position).getUuid()).observe(eventsFragment, event -> {

            if(event != null && addedTickets.indexOf(events.get(position)) == -1) {
                holder.addToggle = false;
                UtilsFunctions.rotateToClose(holder.addRemoveButton);

                if(addedTickets.size() == 0) UtilsFunctions.showPayButton(eventsFragment, payButton);
                addedTickets.add(events.get(position));
            }

            holder.addRemoveButton.setOnClickListener(button -> {
                holder.addToggle = !holder.addToggle;
                toggleAddButton(holder, position);
            });
        });
    }

    private void toggleAddButton(EventHolder holder, int position) {

        if(holder.addToggle) {

            UtilsFunctions.rotateToAdd(holder.addRemoveButton);
            eventSelectedViewModel.deleteByUUID(events.get(position).getUuid());

            addedTickets.remove(events.get(position));
            if(addedTickets.size() == 0) UtilsFunctions.hidePayButton(eventsFragment, payButton);

        } else {

            UtilsFunctions.rotateToClose(holder.addRemoveButton);

            Event event = events.get(position);
            eventSelectedViewModel.insert(new EventSelected(event.getUuid(), event.getName(), event.getDate(), event.getPrice(), event.getImageURL(), 1));
        }
    }

    public void addEvents(List<Event> eventsToInsert) {
        for(int i = 0; i < eventsToInsert.size(); i++) {
            events.add(eventsToInsert.get(i));
            notifyItemInserted(events.indexOf(eventsToInsert.get(i)));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}

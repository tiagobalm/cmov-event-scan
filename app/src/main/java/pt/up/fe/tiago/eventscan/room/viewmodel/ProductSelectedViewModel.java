package pt.up.fe.tiago.eventscan.room.viewmodel;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.entity.ProductSelected;
import pt.up.fe.tiago.eventscan.room.repository.ProductSelectedRepository;

public class ProductSelectedViewModel extends AndroidViewModel {
    private ProductSelectedRepository productSelectedRepository;
    private LiveData<List<ProductSelected>> allProducts;

    public ProductSelectedViewModel(@NonNull Application application) {
        super(application);
        productSelectedRepository = new ProductSelectedRepository(application);
        allProducts = productSelectedRepository.getAllProducts();
    }

    public void insert(ProductSelected productSelected) { productSelectedRepository.insert(productSelected); }
    public void update(ProductSelected productSelected) { productSelectedRepository.update(productSelected); }
    public void delete(ProductSelected productSelected) { productSelectedRepository.delete(productSelected); }
    public void deleteAllProducts() { productSelectedRepository.deleteAllProducts(); }
    public void deleteByUUID(Integer uuid) { productSelectedRepository.deleteByUUID(uuid); }
    public LiveData<List<ProductSelected>> getAllProducts() { return allProducts; }
    public LiveData<ProductSelected> getProductByUUID(Integer uuid) { return productSelectedRepository.getProductByUUID(uuid); }
}

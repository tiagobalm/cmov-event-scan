package pt.up.fe.tiago.eventscan.fragment.start;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import pt.up.fe.tiago.eventscan.MainActivity;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Customer;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.CustomerRepository;
import pt.up.fe.tiago.eventscan.utils.Constants;
import pt.up.fe.tiago.eventscan.utils.watcher.CardNumberWatcher;
import pt.up.fe.tiago.eventscan.utils.watcher.CardValidityWatcher;
import pt.up.fe.tiago.eventscan.utils.adapter.ImageSpinnerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pt.up.fe.tiago.eventscan.utils.UtilsFunctions.showErrorMessage;

public class RegisterSecondFragment extends Fragment {

    @Inject
    CustomerRepository customerRepository;

    private EditText vatNumber;
    private EditText cardNumber;
    private Spinner cardType;
    private EditText cardValidity;

    private String firstName;
    private String lastName;

    private Integer[] images = new Integer[]{
            R.drawable.americanexpress,
            R.drawable.maestro,
            R.drawable.mastercard,
            R.drawable.visa
    };

    private Integer spinnerSelectedValue = R.drawable.americanexpress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);

        firstName = Objects.requireNonNull(getArguments()).getString("First Name");
        lastName = Objects.requireNonNull(getArguments()).getString("Last Name");

        return inflater.inflate(R.layout.register_second, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.go_back).setOnClickListener(button ->
            Navigation.findNavController(view).navigateUp()
        );

        vatNumber = view.findViewById(R.id.vat_number);

        cardNumber = view.findViewById(R.id.card_number);
        cardNumber.addTextChangedListener(new CardNumberWatcher() );

        cardType = view.findViewById(R.id.card_type);
        setAdapterForCardType();

        cardValidity = view.findViewById(R.id.card_validity);
        cardValidity.addTextChangedListener( new CardValidityWatcher() );

        addBlurListenersToFields();

        view.findViewById(R.id.confirm_registration_button).setOnClickListener(button -> validateAndSendRequest());
    }

    private void validateAndSendRequest() {

        if(validateFields()) {

            String[] dateString = cardValidity.getText().toString().split("/");

            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(Calendar.MONTH, Integer.parseInt(dateString[0]));
            calendar.set(Calendar.YEAR,  Integer.parseInt(dateString[1]));
            Date date = calendar.getTime();

            ArrayList<byte[]> keys = generateKeys();
            if(!keys.isEmpty()) {
                Customer customer = new Customer(UUID.randomUUID(), firstName, lastName, Long.parseLong(vatNumber.getText().toString()),
                        getCardTypeString(), Long.parseLong(cardNumber.getText().toString().replace(" ", "")), date, keys.get(0), keys.get(1));

                customerRepository.createCustomer(customer).enqueue(new Callback<UUID>() {
                    @Override
                    public void onResponse(Call<UUID> call, Response<UUID> response) {

                        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getResources().getString(R.string.UUID_TAG), Objects.requireNonNull(response.body()).toString());
                        editor.putString(getResources().getString(R.string.MODULUS_TAG), Base64.encodeToString(keys.get(2), Base64.DEFAULT));
                        editor.putString(getResources().getString(R.string.EXPONENT_TAG), Base64.encodeToString(keys.get(3), Base64.DEFAULT));
                        editor.putString(getResources().getString(R.string.NAME_TAG), String.valueOf(firstName + " " + lastName));
                        editor.apply();

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    @Override
                    public void onFailure(Call<UUID> call, Throwable t) { }
                });
            }
        }
    }

    private ArrayList<byte[]> generateKeys() {
        ArrayList<byte[]> keys = new ArrayList<>();

        try {

            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(Constants.KEY_SIZE);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            RSAPrivateKey privateKey = (RSAPrivateKey)keyPair.getPrivate();
            RSAPublicKey publicKey = (RSAPublicKey)keyPair.getPublic();

            keys.add(privateKey.getModulus().toByteArray());
            keys.add(privateKey.getPrivateExponent().toByteArray());
            keys.add(publicKey.getModulus().toByteArray());
            keys.add(publicKey.getPublicExponent().toByteArray());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return keys;
    }

    private String getCardTypeString() {
        String cardType = "";

        switch (spinnerSelectedValue) {
            case R.drawable.americanexpress:
                cardType = "American Express";
                break;
            case R.drawable.maestro:
                cardType = "Maestro";
                break;
            case R.drawable.mastercard:
                cardType = "MasterCard";
                break;
            case R.drawable.visa:
                cardType = "Visa";
                break;
        }

        return cardType;
    }

    private void setAdapterForCardType() {
        ImageSpinnerAdapter mCustomAdapter = new ImageSpinnerAdapter(getContext(), images);
        cardType.setAdapter(mCustomAdapter);

        cardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView adapterView, View view, int i, long l) {
                spinnerSelectedValue = images[i];
            }

            @Override
            public void onNothingSelected(AdapterView adapterView) {

            }
        });
    }

    private boolean validateFields() {

        if(!(vatNumber.getText().toString().length() == 9)) {
            vatNumber.setError("This field must have 9 digits.");
            return false;
        }

        try {

            if(!(cardNumber.getText().toString().length() == 19)) {
                cardNumber.setError("This field must have 16 digits.");
                return false;
            }

            Long.parseLong(cardNumber.getText().toString().replace(" ",""));

        } catch(NumberFormatException e) {
            cardNumber.setError("This field most only contain digits.");
            return false;
        }

        try {

            if(!(cardValidity.getText().toString().length() == 7)) {
                cardValidity.setError("This field must have 6 digits.");
                return false;
            }

            Integer.parseInt(cardValidity.getText().toString().replace("/",""));

            String[] date = cardValidity.getText().toString().split("/");
            int introducedMonth = Integer.parseInt(date[0]);
            int introducedYear = Integer.parseInt(date[1]);

            if(introducedMonth <= 0 || introducedMonth > 12) {
                cardValidity.setError("The month introduced is not valid.");
                return false;
            }

            if(introducedYear < Calendar.getInstance().get(Calendar.YEAR) || introducedYear > Calendar.getInstance().get(Calendar.YEAR) + 20) {
                cardValidity.setError("The year introduced is not valid.");
                return false;
            }

            Calendar introducedDate = Calendar.getInstance();
            introducedDate.clear();
            introducedDate.set(Calendar.MONTH, introducedMonth);
            introducedDate.set(Calendar.YEAR, introducedYear);

            if(introducedDate.before(Calendar.getInstance())) {
                cardValidity.setError("The card validity introduced is not valid.");
                return false;
            }

        } catch(NumberFormatException e) {
            cardValidity.setError("This field most only contain digits.");
            return false;
        }

        return true;
    }

    private void addBlurListenersToFields() {
        vatNumber.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && vatNumber.getText().toString().equals(""))
                showErrorMessage(vatNumber);
        });

        cardNumber.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && cardNumber.getText().toString().equals(""))
                showErrorMessage(cardNumber);
        });

        cardValidity.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && cardValidity.getText().toString().equals(""))
                showErrorMessage(cardValidity);
        });
    }
}

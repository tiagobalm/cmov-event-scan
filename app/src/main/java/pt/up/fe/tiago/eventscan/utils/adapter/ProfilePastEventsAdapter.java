package pt.up.fe.tiago.eventscan.utils.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Event;
import pt.up.fe.tiago.eventscan.domain.Ticket;
import pt.up.fe.tiago.eventscan.fragment.profile.PastEventsFragment;

public class ProfilePastEventsAdapter extends RecyclerView.Adapter<ProfilePastEventsAdapter.PastEventsHolder> {
    private List<Event> events;
    private List<Ticket> tickets;
    private PastEventsFragment pastEventsFragment;

    static class PastEventsHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventPrice;
        TextView eventDate;
        TextView eventSeat;
        ImageView eventPhoto;
        EditText numberTickets;
        FloatingActionButton addValidate;
        Boolean addToggle;

        PastEventsHolder(View v) {
            super(v);
            eventName = v.findViewById(R.id.event_name);
            eventPrice = v.findViewById(R.id.event_price_input);
            eventDate = v.findViewById(R.id.event_date_input);
            eventSeat = v.findViewById(R.id.event_seat_input);
            eventPhoto = v.findViewById(R.id.event_photo);
            numberTickets = v.findViewById(R.id.number_tickets);
            addValidate = v.findViewById(R.id.add_validate);
            addToggle = true;
        }
    }

    public ProfilePastEventsAdapter(List<Event> events, List<Ticket> tickets, PastEventsFragment pastEventsFragment) {
        this.events = events;
        this.tickets = tickets;
        this.pastEventsFragment = pastEventsFragment;
    }

    @NonNull
    @Override
    public ProfilePastEventsAdapter.PastEventsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_card_item, parent, false);
        return new PastEventsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PastEventsHolder holder, int position) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        holder.eventName.setText(events.get(position).getName());
        holder.eventDate.setText(dateFormat.format(events.get(position).getDate()));
        Glide.with(Objects.requireNonNull(pastEventsFragment.getContext())).load(Uri.parse(events.get(position).getImageURL())).into(holder.eventPhoto);
        holder.eventPrice.setText(String.valueOf(events.get(position).getPrice().toString() + "$"));
        holder.eventSeat.setText(String.valueOf(tickets.get(position).getSeat()));
        holder.addValidate.hide();
    }

    public void addTicketsInformation(List<Event> events, List<Ticket> tickets) {
        for(int i = 0; i < events.size(); i++) {
            this.events.add(events.get(i));
            this.tickets.add(tickets.get(i));
            notifyItemInserted(this.events.indexOf(events.get(i)));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}

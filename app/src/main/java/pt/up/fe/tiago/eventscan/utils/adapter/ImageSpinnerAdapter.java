package pt.up.fe.tiago.eventscan.utils.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import pt.up.fe.tiago.eventscan.R;

public class ImageSpinnerAdapter extends ArrayAdapter<Integer> {

    private Context mContext;
    private Integer[] images;

    public ImageSpinnerAdapter(Context context, Integer[] images) {
        super(context, R.layout.spinner_item_layout);

        this.mContext = context;
        this.images = images;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull  ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {

            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.spinner_item_layout, parent, false);

            viewHolder.cardType = convertView.findViewById(R.id.spinner_image);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.cardType.setImageResource(images[position]);

        return convertView;

    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    private static class ViewHolder {
        ImageView cardType;
    }
}

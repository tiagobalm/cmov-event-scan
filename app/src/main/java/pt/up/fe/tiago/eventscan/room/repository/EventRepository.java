package pt.up.fe.tiago.eventscan.room.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import pt.up.fe.tiago.eventscan.room.EventScanDatabase;
import pt.up.fe.tiago.eventscan.room.dao.EventDao;
import pt.up.fe.tiago.eventscan.room.entity.Event;

public class EventRepository {
    private EventDao eventDao;
    private LiveData<List<Event>> allEvents;

    public EventRepository(Application application) {
        EventScanDatabase eventScanDatabase = EventScanDatabase.getInstance(application);
        eventDao = eventScanDatabase.eventDao();
        allEvents = eventDao.getAllEvents();
    }

    public void insert(Event event) {
        new InsertEvent(eventDao).execute(event);
    }

    public void update(Event event) {
        new UpdateEvent(eventDao).execute(event);
    }

    public void delete(Event event) {
        new DeleteEvent(eventDao).execute(event);
    }

    public LiveData<List<Event>> getAllEvents() {
        return allEvents;
    }

    public LiveData<Event> getEventByUUID(Integer uuid) { return eventDao.getEventByUUID(uuid); }

    public void deleteAllEvents() {
        new DeleteAllEvents(eventDao).execute();
    }

    public void deleteByUUID(Integer uuid) {
        new DeleteByUUID(eventDao).execute(uuid);
    }

    private static class InsertEvent extends AsyncTask<Event, Void, Void> {
        private EventDao eventDao;

        private InsertEvent(EventDao eventDao) {
            this.eventDao = eventDao;
        }

        @Override
        protected Void doInBackground(Event... events) {
            eventDao.insert(events[0]);
            return null;
        }
    }

    private static class UpdateEvent extends AsyncTask<Event, Void, Void> {
        private EventDao eventDao;

        private UpdateEvent(EventDao eventDao) {
            this.eventDao = eventDao;
        }

        @Override
        protected Void doInBackground(Event... events) {
            eventDao.update(events[0]);
            return null;
        }
    }

    private static class DeleteEvent extends AsyncTask<Event, Void, Void> {
        private EventDao eventDao;

        private DeleteEvent(EventDao eventDao) {
            this.eventDao = eventDao;
        }

        @Override
        protected Void doInBackground(Event... events) {
            eventDao.delete(events[0]);
            return null;
        }
    }

    private static class DeleteAllEvents extends AsyncTask<Void, Void, Void> {
        private EventDao eventDao;

        private DeleteAllEvents(EventDao eventDao) {
            this.eventDao = eventDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            eventDao.deleteAllEvents();
            return null;
        }
    }

    private static class DeleteByUUID extends AsyncTask<Integer, Void, Void> {
        private EventDao eventDao;

        private DeleteByUUID(EventDao eventDao) {
            this.eventDao = eventDao;
        }

        @Override
        protected Void doInBackground(Integer... uuid) {
            eventDao.deleteByUUID(uuid[0]);
            return null;
        }
    }
}

package pt.up.fe.tiago.eventscan.utils.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.fragment.events.ShoppingCartFragment;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.TicketRepository;
import pt.up.fe.tiago.eventscan.room.entity.EventSelected;
import pt.up.fe.tiago.eventscan.room.viewmodel.EventSelectedViewModel;
import pt.up.fe.tiago.eventscan.utils.Constants;
import pt.up.fe.tiago.eventscan.utils.UtilsFunctions;
import pt.up.fe.tiago.eventscan.webservice.dto.CreateOrderDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.EventHolder> {
    private List<EventSelected> eventSelecteds;
    private ShoppingCartFragment shoppingCartFragment;
    private EventSelectedViewModel eventSelectedViewModel;
    private Boolean firstAdditionDone;

    @Inject
    Gson gson;

    @Inject
    TicketRepository ticketRepository;

    static class EventHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventPrice;
        TextView eventDate;
        ImageView eventPhoto;
        EditText numberTickets;
        FloatingActionButton addRemoveButton;

        EventHolder(View v) {
            super(v);
            eventName = v.findViewById(R.id.event_name);
            eventPrice = v.findViewById(R.id.event_price_input);
            eventDate = v.findViewById(R.id.event_date_input);
            eventPhoto = v.findViewById(R.id.event_photo);
            numberTickets = v.findViewById(R.id.number_tickets);
            addRemoveButton = v.findViewById(R.id.add_remove_button);
        }
    }

    public ShoppingCartAdapter(List<EventSelected> eventSelecteds, ShoppingCartFragment shoppingCartFragment, FloatingActionButton payButton) {
        this.eventSelecteds = eventSelecteds;
        this.shoppingCartFragment = shoppingCartFragment;
        this.firstAdditionDone = false;

        ((EventScanApplication) Objects.requireNonNull(shoppingCartFragment.getActivity()).getApplication()).getApplicationComponent().inject(this);
        eventSelectedViewModel = ViewModelProviders.of(shoppingCartFragment).get(EventSelectedViewModel.class);

        payButton.setOnClickListener(button -> sendOrderRequest());
    }

    @NonNull
    @Override
    public ShoppingCartAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card_item, parent, false);
        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, int position) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        holder.eventName.setText(eventSelecteds.get(position).getName());
        holder.eventDate.setText(dateFormat.format(eventSelecteds.get(position).getDate()));
        Glide.with(Objects.requireNonNull(shoppingCartFragment.getContext())).load(Uri.parse(eventSelecteds.get(position).getImageURL())).into(holder.eventPhoto);
        holder.eventPrice.setText(String.valueOf(eventSelecteds.get(position).getPrice().toString() + "$"));
        holder.numberTickets.setVisibility(View.VISIBLE);
        holder.numberTickets.setText(String.valueOf(eventSelecteds.get(position).getNumberOfTickets()));
        holder.numberTickets.addTextChangedListener(EditTicketNumberTextWatcher(position));

        UtilsFunctions.rotateToClose(holder.addRemoveButton);

        holder.addRemoveButton.setOnClickListener(button -> removeEventFromDatabase(position));
    }

    private TextWatcher EditTicketNumberTextWatcher(int position) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    eventSelecteds.get(position).setNumberOfTickets(Integer.parseInt(s.toString()));
                    eventSelectedViewModel.update(eventSelecteds.get(position));
                }
            }
        };
    }

    private void sendOrderRequest() {
        List<pt.up.fe.tiago.eventscan.domain.Event> eventsToSend = new ArrayList<>();
        List<Integer> numberOfTickets = new ArrayList<>();

        for(EventSelected eventSelected : eventSelecteds) {
            pt.up.fe.tiago.eventscan.domain.Event sendEvent  = new pt.up.fe.tiago.eventscan.domain.Event(eventSelected.getName(),
                    eventSelected.getDate(), eventSelected.getPrice(), eventSelected.getImageURL());
            sendEvent.setUuid(eventSelected.getUuid());
            eventsToSend.add(sendEvent);
            numberOfTickets.add(eventSelected.getNumberOfTickets());
        }

        try {
            Signature sig = Signature.getInstance(Constants.SIGN_ALGORITHM);
            SharedPreferences sharedPreferences = shoppingCartFragment.getActivity()
                    .getSharedPreferences(Objects.requireNonNull(shoppingCartFragment.getActivity()).getResources().getString(R.string.STORED_FILE),
                            Context.MODE_PRIVATE);

            String UUID = sharedPreferences.getString(shoppingCartFragment.getActivity().getResources().getString(R.string.UUID_TAG), "");
            byte[] keyModulus = Base64.decode(sharedPreferences.getString(shoppingCartFragment.getActivity().getResources().getString(R.string.MODULUS_TAG), ""), Base64.DEFAULT);
            byte[] keyExponent = Base64.decode(sharedPreferences.getString(shoppingCartFragment.getActivity().getResources().getString(R.string.EXPONENT_TAG), ""), Base64.DEFAULT);

            RSAPrivateKeySpec RSASpec = new RSAPrivateKeySpec(new BigInteger(keyModulus), new BigInteger(keyExponent));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = factory.generatePrivate(RSASpec);

            JSONArray dataToSend = new JSONArray();
            dataToSend.put(UUID);

            JSONObject eventsJSON = new JSONObject();
            eventsJSON.put("eventSelecteds", gson.toJson(eventsToSend));
            dataToSend.put(eventsJSON);

            JSONObject numberOfTicketsJSON = new JSONObject();
            numberOfTicketsJSON.put("numberOfTickets", gson.toJson(numberOfTickets));
            dataToSend.put(numberOfTicketsJSON);

            sig.initSign(privateKey);
            sig.update(dataToSend.toString().getBytes(StandardCharsets.UTF_8));
            byte[] signatureBytes = sig.sign();

            CreateOrderDTO createOrderDTO = new CreateOrderDTO(signatureBytes, dataToSend.toString().getBytes(StandardCharsets.UTF_8));

            ticketRepository.createOrder(createOrderDTO).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    if(Objects.equals(response.body(), Constants.SUCCESS_STRING)) {
                        eventSelectedViewModel.deleteAllEvents();
                        Navigation.findNavController(Objects.requireNonNull(shoppingCartFragment.getView())).navigateUp();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) { System.out.println("FAILURE: " + t.getMessage());}
            });

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void removeEventFromDatabase(int position) {

        eventSelectedViewModel.deleteByUUID(eventSelecteds.get(position).getUuid());
        eventSelecteds.remove(eventSelecteds.get(position));
        notifyItemRemoved(position);

        if(eventSelecteds.size() == 0)
            Navigation.findNavController(Objects.requireNonNull(shoppingCartFragment.getView())).navigateUp();
    }

    public void addEvents(List<EventSelected> eventsToInsert) {

        if(!firstAdditionDone) {
            for (int i = 0; i < eventsToInsert.size(); i++) {
                eventSelecteds.add(eventsToInsert.get(i));
                notifyItemInserted(eventSelecteds.indexOf(eventsToInsert.get(i)));
            }
            firstAdditionDone = true;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return eventSelecteds.size();
    }
}

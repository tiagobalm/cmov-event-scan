package pt.up.fe.tiago.eventscan.fragment.vouchers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.domain.Voucher;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.VoucherRepository;
import pt.up.fe.tiago.eventscan.utils.adapter.VouchersAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VouchersFragment extends Fragment {

    @Inject
    VoucherRepository voucherRepository;

    private int currentIndex = 0;
    private int sizePagination = 18;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);

        return inflater.inflate(R.layout.vouchers_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NestedScrollView nestedScrollView = view.findViewById(R.id.vouchers_nested_scroll_view);
        RecyclerView vouchersRecyclerView = view.findViewById(R.id.vouchers_recycleview);
        ProgressBar progressBar = view.findViewById(R.id.vouchers_loading);
        ViewCompat.setNestedScrollingEnabled(vouchersRecyclerView, false);
        vouchersRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        VouchersAdapter vouchersAdapter = new VouchersAdapter(new ArrayList<>());
        vouchersRecyclerView.setAdapter(vouchersAdapter);

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);
        voucherRepository.getAllUnusedVouchers(UUID.fromString(sharedPreferences.getString(getResources().getString(R.string.UUID_TAG), ""))).enqueue(new Callback<List<Voucher>>() {
            @Override
            public void onResponse(Call<List<Voucher>> call, Response<List<Voucher>> response) {
                List<Voucher> vouchers = response.body();
                List<Voucher> firstVouchers;

                if(Objects.requireNonNull(vouchers).size() < sizePagination) {
                    firstVouchers = vouchers.subList(currentIndex, vouchers.size());
                    currentIndex = vouchers.size();
                } else {
                    firstVouchers = vouchers.subList(currentIndex, sizePagination);
                    currentIndex += sizePagination;
                }

                vouchersAdapter.addVouchers(firstVouchers);
                progressBar.setVisibility(View.GONE);
                vouchersRecyclerView.setVisibility(View.VISIBLE);

                nestedScrollView.fullScroll(ScrollView.FOCUS_UP);
                nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            List<Voucher> restVouchers;

                            if(currentIndex != vouchers.size()) {
                                if ((currentIndex + sizePagination) < vouchers.size()) {
                                    restVouchers = vouchers.subList(currentIndex, currentIndex + sizePagination);
                                    currentIndex += sizePagination;
                                } else {
                                    restVouchers = vouchers.subList(currentIndex, vouchers.size());
                                    currentIndex = vouchers.size();
                                }

                                vouchersAdapter.addVouchers(restVouchers);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Voucher>> call, Throwable t) {}
        });
    }

    @Override
    public void onPause() {
        currentIndex = 0;
        super.onPause();
    }
}

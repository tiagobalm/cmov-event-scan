package pt.up.fe.tiago.eventscan.fragment.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.eventscan.R;
import pt.up.fe.tiago.eventscan.initializer.EventScanApplication;
import pt.up.fe.tiago.eventscan.repository.CafeteriaRepository;
import pt.up.fe.tiago.eventscan.utils.RecyclerViewDecoration;
import pt.up.fe.tiago.eventscan.utils.adapter.ProfileCafeteriaAdapter;
import pt.up.fe.tiago.eventscan.webservice.dto.CafeteriaOrderResultDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviousCafeteriaFragment extends Fragment {

    @Inject
    CafeteriaRepository cafeteriaRepository;

    private ProfileCafeteriaAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((EventScanApplication) Objects.requireNonNull(getActivity()).getApplication()).getApplicationComponent().inject(this);
        return inflater.inflate(R.layout.profile_cafeteria, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView mRecyclerView = view.findViewById(R.id.profile_cafeteria_recycleview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(30));
        mAdapter = new ProfileCafeteriaAdapter(new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(getResources().getString(R.string.STORED_FILE), Context.MODE_PRIVATE);

        cafeteriaRepository.getAllPastOrders(UUID.fromString(sharedPreferences.getString("UUID", ""))).enqueue(new Callback<List<CafeteriaOrderResultDTO>>() {
            @Override
            public void onResponse(Call<List<CafeteriaOrderResultDTO>> call, Response<List<CafeteriaOrderResultDTO>> response) {
                mAdapter.addOrderInformation(response.body());
            }

            @Override
            public void onFailure(Call<List<CafeteriaOrderResultDTO>> call, Throwable t) {}
        });

        super.onViewCreated(view, savedInstanceState);
    }
}


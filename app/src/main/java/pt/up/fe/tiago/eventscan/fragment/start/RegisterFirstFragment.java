package pt.up.fe.tiago.eventscan.fragment.start;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.navigation.Navigation;
import pt.up.fe.tiago.eventscan.R;

import static pt.up.fe.tiago.eventscan.utils.UtilsFunctions.showErrorMessage;

public class RegisterFirstFragment extends Fragment {

    private EditText firstName;
    private EditText lastName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firstName = view.findViewById(R.id.first_name);
        lastName = view.findViewById(R.id.last_name);
        FloatingActionButton nextButton = view.findViewById(R.id.next_phase);

        addBlurListenersToFields();

        nextButton.setOnClickListener(button -> {
            if(validateFields()) {

                Bundle bundleName = new Bundle();
                bundleName.putString("First Name", firstName.getText().toString());
                bundleName.putString("Last Name", lastName.getText().toString());

                Navigation.findNavController(view).navigate(R.id.action_registerFirstFragment_to_registerSecondFragment, bundleName);
            }
        });
    }

    private void addBlurListenersToFields() {
        firstName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && firstName.getText().toString().equals(""))
                showErrorMessage(firstName);
        });

        lastName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && lastName.getText().toString().equals(""))
                showErrorMessage(lastName);
        });
    }

    private boolean validateFields() {

        if(firstName.getText().toString().equals("")) {
            showErrorMessage(firstName);
            return false;
        }

        if(lastName.getText().toString().equals("")) {
            showErrorMessage(lastName);
            return false;
        }

        return true;
    }


}
